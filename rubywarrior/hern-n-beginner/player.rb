class Player
  
  def play_turn(warrior)
    if warrior.feel.wall?
      @captives_rescued = 1
      warrior.pivot!
    else
      if being_attacked?(warrior)
        attack_or_retreat!(warrior)
      else
        if captives_rescued?
          move_or_interact_forward!(warrior) 
        else
          move_or_interact_backward!(warrior)  
        end
      end
    end  
  end

  private 
  def being_attacked?(warrior) 
  	@health = warrior.health if @health.nil?

    health = warrior.health
    
  	if @health == health	
  		return false
  	else
  		@health = health
  		return true
  	end	

  end

  private
  def captives_rescued?
    @captives_rescued = 0 if @captives_rescued.nil?
    
    if @captives_rescued > 0
      return true
    else
      return false
    end
  end

  private
  def shoot_or_walk(warrior)
    action = :walk!
    warrior.look.each do |space|
      
      if space.captive?
        action = :walk!
        break
      end
      
      if space.enemy?
        action = :shoot!
        break
      end
    
    end
    warrior.method(action).call

  end

  private
  def attack_or_retreat!(warrior)
    health = warrior.health	
    if warrior.feel.empty?
      if health < 8   
        warrior.walk!(:backward)
      else
        shoot_or_walk(warrior)
      end
    else
  		warrior.attack!
    end	
  end

  private
  def move_or_interact_forward!(warrior)
    health = warrior.health
    if warrior.feel.empty?  
      if health < 14
        warrior.rest!
        @health += 2
      else  
        shoot_or_walk(warrior)
      end
    else
      if warrior.feel.captive?
        warrior.rescue!
        @captives_rescued += 1       
      else
        warrior.attack!
      end
    end
  end

  private
  def move_or_interact_backward!(warrior)
    health = warrior.health
    if warrior.feel(:backward).empty?
      if health < 14
        warrior.rest!
        @health += 2
      else
        warrior.walk!(:backward)
      end
    else
      if warrior.feel(:backward).captive?
        warrior.rescue!(:backward)
        @captives_rescued += 1
      else
        if warrior.feel(:backward).wall?
          @captives_rescued += 1
          move_or_interact_forward!(warrior)    
        else  
          warrior.attack!(:backward)
        end
      end
    end
  end

end